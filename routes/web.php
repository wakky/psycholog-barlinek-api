<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Controllers\PageController;
use App\Http\Controllers\ArticleController;


$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'v1.0.1'], function() use ($router) {
    $router->get('pages[/{id}]', ['as' => 'pages', 'uses' => 'PageController@show']);
    $router->post('pages', ['middleware' => 'auth', 'uses' => 'PageController@create']);
    $router->delete('pages/{id}', ['middleware' => 'auth', 'uses' => 'PageController@delete']);
    $router->put('pages/{id}', ['middleware' => 'auth', 'uses' => 'PageController@update']);

    $router->get('articles[/{id}]', ['uses' => 'ArticleController@show']);
    $router->post('articles', ['middleware' => 'auth', 'uses' => 'ArticleController@create']);
    $router->delete('articles/{id}', ['middleware' => 'auth', 'uses' => 'ArticleController@delete']); 
    $router->put('articles/{id}', ['middleware' => 'auth', 'uses' => 'ArticleController@update']);

    $router->post('login', ['uses' => 'UserController@login']);
});